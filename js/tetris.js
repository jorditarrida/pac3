// Referencia del codi:
var COLS = 15, ROWS = 30;
var board = [];
var endOfGame = false;
var interval;
var user_name = "TEDW-PRA2"; // Nom de l'usuari per a possar-ho a les millors puntuacions.
// Nivell d'inici inicialitzat a 0.
var level = 0;

// Durada en mil·lisegons de cada iteració del joc.
// Quan l'usuari pugi de nivell, la durada de l'interval disminuirà.
var interval_ms = 550;

// Punts acumulats per l'usuari. Cada 1000 punts l'usuari pujarà de nivell.
var user_points = 0;

// Figura actual que s esta desplaçant.
var current; // current moving shape
var currentX, currentY; // position of current shape

// Figura de la següent forma que es mostrara.
var nextShape;

var pause = false;

// Formes que poden prendre les figures.
var shapes = [
    [ 1, 1, 1, 1 ],
    [ 1, 1, 1, 0,
      1 ],
    [ 1, 1, 1, 0,
      0, 0, 1 ],
    [ 1, 1, 0, 0,
      1, 1 ],
    [ 1, 1, 0, 0,
      0, 1, 1 ],
    [ 0, 1, 1, 0,
      1, 1 ],
    [ 0, 1, 0, 0,
      1, 1, 1 ]
];
// Colors  de les figures. Cada figura te un color asignat.
var colors = [
    'cyan', 'orange', 'blue', 'yellow', 'red', 'green', 'purple'
];


// Crea una nova figura 4x4 en la variable global 'current'
// La figura és 4x4 per tindre espai suficient quan es roti la figura.
function newShape() {
    var id = Math.floor( Math.random() * shapes.length );
    var shape = shapes[ id ]; // maintain id for color filling

	var auxShape;
	
    auxShape = [];
    for ( var y = 0; y < 4; ++y ) {
        auxShape[ y ] = [];
        for ( var x = 0; x < 4; ++x ) {
            var i = 4 * y + x;
            if ( typeof shape[ i ] != 'undefined' && shape[ i ] ) {
                auxShape[ y ][ x ] = id + 1;
            }
            else {
                auxShape[ y ][ x ] = 0;
            }
        }
    }
	// Inicialitzem de nou les coordenades de sortida de la figura.
    currentX = 3;
    currentY = 0;
	return auxShape;
}

// Inicialitzem el tauler possant tots els seus elements a 0.
function init() {
	for ( var y = 0; y < ROWS; ++y ) {
        board[ y ] = [];
        for ( var x = 0; x < COLS; ++x ) {
            board[ y ][ x ] = 0;
        }
    }
}

// Atura la figura a la seva posició i la guarda al taulell.
function freeze() {
    for ( var y = 0; y < 4; ++y ) {
        for ( var x = 0; x < 4; ++x ) {
            if ( current[ y ][ x ] ) {
                board[ y + currentY ][ x + currentX ] = current[ y ][ x ];
            }
        }
    }
}

// retorna la posició de la figura actual rotada perpendicularment en sentit contrari a les agulles del rellotge.
function rotate( current ) {
	
	var newCurrent = [];
	for ( var y = 0; y < 4; ++y ) {
		newCurrent[ y ] = [];
		for ( var x = 0; x < 4; ++x ) {
			newCurrent[ y ][ x ] = current[ 3 - x ][ y ];
		}
	}
	
    return newCurrent;
}

/* 
	Verifica si alguna linia està plena i la neteja.
	Exercici: En aquesta funció s'haurà d'emetre el so de fitxa.ogg
*/
function clearLines() {	
    for ( var y = ROWS - 1; y >= 0; --y ) { // recorregut linia per linia
        var rowFilled = true;
        for ( var x = 0; x < COLS; ++x ) { // mirem cada cel·la de la linia.
            if ( board[ y ][ x ] == 0 ) { //si el valor de la cel·la és 0, la linia no està plena. Passem a la següent.
                rowFilled = false;
                break;
            }
        }
		// Si la linia està plena, l'esborrem.
        if ( rowFilled ) {			            
        	document.getElementById('clearsound').play();
			// Emetem el so de linia plena.
            for ( var yy = y; yy > 0; --yy ) {
                for ( var x = 0; x < COLS; ++x ) {
                    board[ yy ][ x ] = board[ yy - 1 ][ x ];
                }
            }
            ++y;
        }
    }
}

/* 
	Gestiona la tecles del teclat premudes.
	Exercici:  s'ha de tindre en compte si el joc està en pausa.
*/
function keyPress( key ) {
    switch ( key ) {
        case 'left':			
			if ( valid( -1 ) ) {
				--currentX;
			}			
            break;
			
        case 'right':			
			if ( valid( 1 ) ) {
				++currentX;
			}
			
			break;
				
        case 'down':			
			if ( valid( 0, 1 ) ) {
				++currentY;
			}			
            break;
			
        case 'rotate':
			
			var rotated = rotate( current );
			if ( valid( 0, 0, rotated ) ) {
				current = rotated;
			}					
            break;

        case 'pause':
			pause = !pause;			
            break;		
    }
}

// checks if the resulting position of current shape will be feasible
// verifica que la posició actual sigui valida.
function valid( offsetX, offsetY, newCurrent ) {	

	// Inicialitzem el valor per defecte de les variables si aquestes no s'han passar com a paràmetre.
    offsetX = offsetX || 0;
    offsetY = offsetY || 0;
	newCurrent = newCurrent || current;
	
	// Calculem la nova posició de la figura. ( posició actual + desplaçament ) per a cada coordenada.
    offsetX = currentX + offsetX;
    offsetY = currentY + offsetY;
    


	// Mirem si es compleixen alguna de les condicions per a les quals el moviment no seria vàlid. Les possiblitats són:
	//	Ja hi ha una peça en aquesta posició
	//		board[ y + offsetY ][ x + offsetX ]
	//
	//	La nova posició no es troba dins de taulell del canvas.
	//		typeof board[ y + offsetY ] == 'undefined'
	//		typeof board[ y + offsetY ][ x + offsetX ] == 'undefined'
	//		
	//  Algna coordenada surt dels marges del canvas :  
	//		x + offsetX < 0 
	//		x + offsetX >= COLS
	//		y + offsetY >= ROWS
	//
	// (offsetY == 1) => La figura es troba a la part superior del canvas. Hi ha fitxes fins adalt de tot del canvas.

    for ( var y = 0; y < 4; ++y ) {
        for ( var x = 0; x < 4; ++x ) {
            if ( newCurrent[ y ][ x ] ) {
                if ( typeof board[ y + offsetY ] == 'undefined'
                  || typeof board[ y + offsetY ][ x + offsetX ] == 'undefined'
                  || board[ y + offsetY ][ x + offsetX ]
                  || x + offsetX < 0
                  || y + offsetY >= ROWS
                  || x + offsetX >= COLS ) {
						// A més, si la coordenada Y de la figura es troba a la part superior, hem acabat la partida.
						if (offsetY == 1) { 
							endOfGame = true; 
						} 
                    return false;
                }
            }
        }
    }
    return true;
}

/* 
	Desplaça la figura cap avall, crea noves figures i neteja linies.
	Exercicis: 
		S'ha de tindre en comtpe si el joc està en pausa.
		S'ha de tindre en compte si el joc ha acabat per mostrar el missatge.
		S'ha d'actualitzar la puntuació de l'usuari i el nivell en el que es troba.
		Si es canvia de nivell s'haurà d'actualitzar la frequència ( variable interval_ms ) de desplaçament de les fitxes.
		Si s'acaba el joc s'haurà de controlar si la partida de l'usuari forma part de les millors partides.
*/

function drawGame() {

	// sortim de la funció si el joc està en pausa
	if( pause ) return;

		// Verifiquem si la figura pot ser desplaçada o si s'ha acabat el joc.
		if ( valid( 0, 1 ) ) {
			++currentY;
			
		// Si no s'ha acabat el joc i la figura tampoc ser desplaçada cap avall, significa que la figura s'ha posicionat a sobre d'un altre figura.
		}else{ 
			freeze(); // Congelem la figura a la seva posició actual.
			clearLines(); // Mirem si es pot netejar alguna linia.			
			
			if ( endOfGame == false ){
				// Haurem d'actualitzar la puntuació de l'usuari i mostrar-la al costat dret del joc.
				// Comprovem si s'ha de canviar de nivell.
				// Aquestes dues tasques es resolen amb la funció addPoints(n)
				addPoints(10);
				
				
			}else{ // si el joc s'ha acabat verifiquem si l'usuari ha obtingut una de les millors posicions.
				// Aquesta tasca es resol amb la funció addScore()
				addScore();
			}		
			
			if ( endOfGame == false ){			
				
				/*
				Exercici: 
					La següent linia  sense comentar "current = newShape();"
					està possada per poder continuar amb el joc sense tindre la "next Shape" indicada.
					Quan s'introdueixi la nextShape, aqesta linia s'haura de reemplaçar per altres.
				*/
				current = nextShape;
				nextShape = newShape();
				
				currentX = 3;
				currentY = 0;				
			}
			// Actualitzem el canvas on es mostra la següent figura.			
		}
		
		// Exercici: Controlem si el joc ha acabat.		
		if( endOfGame ){
			endGame();
		} else {
			drawNextShape();
		}
				
}

// Afegir punts i actualitzar nivell
function addPoints( points ) {
	// afegir punts i actualitzar DOM
	user_points += points;
	document.getElementById('points').innerHTML = user_points;
	// modificar nivell si s'escau i actualitzar DOM
	if( user_points % 100 == 0 ){
		level++;
		// modificar interval_ms si interval_ms > 0
		if( interval_ms > 0 ) {
			interval_ms -= 50;
		}
		clearInterval(interval);
		interval = setInterval( drawGame, interval_ms );
		document.getElementById('level').innerHTML = level;
	}
}


/* Acabament del joc
	- dibuixa missatge d'acabament drawEndOfGame
	- mostra botó nou joc
	- elimina l'interval
*/
function endGame(){
	drawEndOfGame();
	document.getElementById('newgame').style.display = "block";
	document.getElementById('newgame').addEventListener('click', newGame);
	clearInterval(interval);
}

/* Carrega les puntuacions
	des de /src/users.php i les pinta al DOM
*/
function loadScores(){

	var request = new XMLHttpRequest();
	request.open('GET', '/src/users.php', true);

	request.onload = function() {
	  if (request.status >= 200 && request.status < 400) {
	    var resp = JSON.parse(request.responseText);
	    var ul = document.getElementById('best_scores_content').firstElementChild;
	    ul.innerHTML = "";
	    for( var user in resp ){
	    	var element = document.createElement('li');
	    	element.setAttribute('data-points', resp[user]['points']);
	    	element.innerHTML = resp[user]['name'] + "<br>Level: " + resp[user]['level'] + "<br>Points: " + resp[user]['points'];
	    	ul.appendChild(element);
	    }
	  } else {
	    console.error("S'ha produït un error en recuperar les puntuacions desades.");
	  }
	};

	request.onerror = function() {
	  console.error("S'ha produït un error en recuperar les puntuacions desades.");
	};

	request.send();

}

/* Afegeix la puntuació actual a la llista si és més gran que alguna de les presents
*/
function addScore(){
	var ul = document.getElementById('best_scores_content').firstElementChild;
	var elements = ul.children;
	for( var i = 0; i < elements.length; i++ ){
		if( user_points >= elements[i].getAttribute('data-points') ){
			var element = document.createElement('li');
	    	element.setAttribute('data-points', user_points);
	    	element.innerHTML = user_name + "<br>Level: " + level + "<br>Points: " + user_points;
	    	ul.insertBefore( element, elements[i]);
			return;
		}
	}
}


/* Inicialitzem una nova partida 
	Exercicis: 
		S'ha de dibuixar la segúent figura.
*/

function newGame() {
	// reiniciem totes les variables i el DOM
    clearInterval(interval);
    endOfGame = false;	
    pause = false;
    saved = false;
    level = 0;
    document.getElementById('level').innerHTML = level;
    user_points = 0;
    document.getElementById('points').innerHTML = user_points;
    interval_ms = 550;
	current = newShape(); // Creem una segona figura per a tenir la figura actual i la figura següent.
	nextShape = newShape();
	init();
	drawNextShape();
    interval = setInterval( drawGame, interval_ms );
	document.getElementById('newgame').style.display = "none";
	document.getElementById('newgame').removeEventListener('click', newGame);
}

// canviem com inicialitzem el joc: només quan el document està carregat
// equivalent a jQuery(document).ready()
function initGame(){
    loadScores();
    newGame();
}


function ready() {
  if (document.readyState != 'loading'){
    initGame();
  } else {
    document.addEventListener('DOMContentLoaded', initGame);
  }
}

ready();
