var nextShapeCanvas = document.getElementsByTagName( 'canvas' )[ 0 ];
var canvas = document.getElementsByTagName( 'canvas' )[ 1 ];
//var canvas = document.getElementByID( 'canvas_game' );

var ctx = canvas.getContext( '2d' );
var W = 300, H = 450;
var BLOCK_W = W / COLS, BLOCK_H = H / ROWS;

var ctxNextShape = nextShapeCanvas.getContext('2d');



// Dibuixa un quadre.
function drawBlock( x, y ) {
    ctx.fillRect( BLOCK_W * x, BLOCK_H * y, BLOCK_W - 1 , BLOCK_H - 1 );
    ctx.strokeRect( BLOCK_W * x, BLOCK_H * y, BLOCK_W - 1 , BLOCK_H - 1 );
}

// Dibuixa el canvas i la fitxa en desplaçament
function render() {
	// netegem el canvas actual.
    ctx.clearRect( 0, 0, W, H );

	// Dibuixem el canvas sencer. 
    ctx.strokeStyle = 'black';
	
    for ( var x = 0; x < COLS; ++x ) { // Primer dibuixem totes les cel·les d'una fila. 
        for ( var y = 0; y < ROWS; ++y ) { // Fem un recorregut fila per fila.
            if ( board[ y ][ x ] ) {
				// Actualitzem el color abans de demanar que es pinti el quadre.
                ctx.fillStyle = colors[ board[ y ][ x ] - 1 ];
                drawBlock( x, y );
            }
        }
    }
		
	// Dibuixem la figura en moviment.
    ctx.fillStyle = 'red';
    ctx.strokeStyle = 'black';
    for ( var y = 0; y < 4; ++y ) {
        for ( var x = 0; x < 4; ++x ) {
            if ( current[ y ][ x ] ) {
                ctx.fillStyle = colors[ current[ y ][ x ] - 1 ];				
                drawBlock( currentX + x, currentY + y );
            }
        }
    }
		
}

/* Dibuixa la següent fitxa.
	El fitxer de moment només dibuixa el text "Next Shape".
	Exercici: S'ha de dibuixar al costat del text la següent fitxa.
*/
function drawNextShape(){
	
	ctxNextShape.clearRect( 0, 0, W, H );
	
	// Dibuixem el text de Next Shape.
	ctxNextShape.fillStyle = 'black';	
	ctxNextShape.font="20px Arial";
	ctxNextShape.fillText("Next Shape:",20,35);

    // dibuixem la figura següent
    ctxNextShape.fillStyle = 'red';
    ctxNextShape.strokeStyle = 'black';
    for ( var y = 0; y < 4; ++y ) {
        for ( var x = 0; x < 4; ++x ) {
            if ( nextShape[ y ][ x ] ) {
                ctxNextShape.fillStyle = colors[ nextShape[ y ][ x ] - 1 ];
                var pos_x = x + 7,
                    pos_y = y + 1;
                ctxNextShape.fillRect( BLOCK_W * pos_x, BLOCK_H * pos_y, BLOCK_W - 1 , BLOCK_H - 1 );
                ctxNextShape.strokeRect( BLOCK_W * pos_x, BLOCK_H * pos_y, BLOCK_W - 1 , BLOCK_H - 1 );
            }
        }
    }

}

/* Dibuixa el final del joc
*/
function drawEndOfGame(){
    ctxNextShape.clearRect( 0, 0, W, H );
    
    // Dibuixem el text de Next Shape.
    ctxNextShape.fillStyle = 'black';   
    ctxNextShape.font="20px Arial";
    ctxNextShape.fillText("Game Over",20,35);
}

// El joc es dibuixa cada 30 milisegons. Aquest interval és independent
setInterval( render, 30 );
