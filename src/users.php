<?php 

$users = array(
    array(
		"name" => "Best User 1", 
		"level" => "100070",
		"points" => "1000"		
    ),

    array(
		"name" => "Best User 2", 
		"level" => "10080",
		"points" => "100"		
    ),
	
	array(
		"name" => "Best User 3", 
		"level" => "5040",
		"points" => "50"		
    ),

    array(
		"name" => "Best User 4", 
		"level" => "1090",
		"points" => "10"		
    ),


    array(
		"name" => "Best User 5", 
		"level" => "90",
		"points" => "0"		
    ),

    array(
		"name" => "#ccc", 
		"level" => "0",
		"points" => "0"		
    )	
	
);


echo json_encode($users);

?>
